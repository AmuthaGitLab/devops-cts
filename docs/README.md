# Building Hello World
This Repository will demonstrate how to build Docker image of Go HelloWorld app and also shows how can be verified using Docker Compose
## Step1: Go files
Under the directory ./cmd/pinger we have written GOlang files which basically creates PING service

## Step2: Build Docker image
Under the directory ./deployments/build, we have created Dockerfile which basically creates the docker image of this directory

## Step3: Gitlab CI
We also have created Gitlab CI which builds the dockerimage whenever you committed the code into Repository. Also it creates tar file which you can load into your private Repository if you want

## Step4: Docker Compose
We also created Docker Compose and that file creates two container and ping each other which proves that our image works fine
